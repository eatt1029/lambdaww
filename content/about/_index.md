+++
title = "About"
date = "2020-01-04T17:39:21-07:00"
aliases = ["about-us", "contact"]
author = "Lambdaww"
enableComments = false
+++

We are a development team located in **Taiwan**; and, focus on *providing software development service* and *enabling IoT solution* for remote and globol requirement.

![](awards.png)

We specialize in solving problem with [**formal methods**](https://en.wikipedia.org/wiki/Formal_methods) and [**functional programming**](https://en.wikipedia.org/wiki/Functional_programming).
